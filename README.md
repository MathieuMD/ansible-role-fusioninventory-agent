Ansible Role FusionInventory-Agent
=========

Install and configure a [FusionInventory Agent](http://fusioninventory.org/documentation/documentation.html).

Role Variables
--------------

FusionInventory Agent mode is by default set to `cron`, via variable `fiagent_mode`.

Remote server is defined in `fiagent_server.server`.

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
  - role: ansible-role-fusioninventory-agent
    fiagent_server:
      server: "https://test.example.com/"
      user: "test_user"
      password: "test_password"
```

License
-------

GPLv3
